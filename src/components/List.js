import React, {Component} from 'react';
import Item from "./Item";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleGetItem = this.handleGetItem.bind(this);
  }

  handleShowFilter() {
    const {items, filterStatus} = this.props;
    let elmItem = [];
    if (filterStatus === 'active') {
      elmItem = items
        .filter(item => item.complete === false)
        .map((item, index) => {
          return (
            <Item onClickComplete={this.props.onClickComplete} onClickDelete={this.props.onClickDelete} key={index}
                  item={item}/>
          );
        });
    }
    else if (filterStatus === 'completed') {
      elmItem = items
        .filter(item => item.complete === true)
        .map((item, index) => {
          return (
            <Item onClickComplete={this.props.onClickComplete} onClickDelete={this.props.onClickDelete} key={index}
                  item={item}/>
          );
        });
    }
    else {
      elmItem = items.map((item, index) => {
        return (
          <Item onClickComplete={this.props.onClickComplete} onClickDelete={this.props.onClickDelete} key={index}
                item={item}/>
        );
      })
    }
    return elmItem;
  }

  handleGetItem(arr) {
    // let count = true;
    this.props.onClickSetCompleted(arr);
  }

  render() {
    const {items} = this.props;
    return (
      <section id="main">
        <input id="toggle-all" type="checkbox" onClick={() => this.handleGetItem(items)}/>
        <label htmlFor="toggle-all">Mark all as complete</label>
        <ul id="todo-list">
          {this.handleShowFilter()}
        </ul>
      </section>
    );
  }
}

export default List;
