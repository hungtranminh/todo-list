import React, {Component} from 'react';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      content: event.target.value
    });
  }

  handleSubmit(event) {
    const check = this.state.content.trim();
    if (check) {
      let item = {
        content: this.state.content,
        complete: false
      };
      this.props.onClickSubmit(item);
      this.setState({
        content: '',
      });
      event.preventDefault();
    }
    event.preventDefault();
  }

  render() {
    return (
      <header id="header">
        <h1> todos </h1>
        <form id="todo-form" onSubmit={this.handleSubmit}>
          <input id="new-todo" type={"text"} value={this.state.content} onChange={this.handleChange} name={"content"}
                 placeholder="What needs to be done?" autoFocus/>
          <input type="submit" hidden={" "}/>
        </form>
      </header>
    );
  }
}

export default Form;
