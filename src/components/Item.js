import React, {Component} from 'react';

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleDelete = this.handleDelete.bind(this);
  }


  handleDelete(id) {
    this.props.onClickDelete(id);
  }

  handleComplete(item) {
    this.props.onClickComplete(item);
  }

  render() {
    // console.log(this.props.item);
    const item = this.props.item;
    let check;
    if (this.props.item.complete === true)
      check = 'completed';
    return (
      <li className={check}>
        <div className="view">
          <input onChange={() => this.handleComplete(item)} className="toggle" type="checkbox"
                 checked={this.props.item.complete}/>
          <label>{item.content}</label>
          <button onClick={() => this.handleDelete(item.id)} className="destroy"/>
        </div>
        <form>
          <input className="edit"/>
        </form>
      </li>
    );
  }
}

export default Item;
