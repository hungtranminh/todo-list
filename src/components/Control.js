import React, {Component} from 'react';

class Control extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.countItemActive = this.countItemActive.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleClearCompleted = this.handleClearCompleted.bind(this);
  }

  countItemActive() {
    const countItems = this.props.items.filter(item => item.complete === false);
    return countItems.length;
  }

  handleFilter(filterStatus) {
    this.props.onClickFilterStatus(filterStatus);
  }

  handleGetItemCompleted() {
    const countItems = this.props.items.filter(item => item.complete === true);

    if (countItems.length >= 1) {
      return (
        <button id="clear-completed" onClick={() => this.handleClearCompleted(countItems)}>Clear completed</button>
      );
    }
  }

  handleClearCompleted(arr) {
    this.props.onClickClearCompleted(arr);
  }

  render() {
    console.log();
    let al, com, act;
    if (this.props.filterStatus === 'all') {
      al = 'selected';
    }
    else if (this.props.filterStatus === 'active') {
      act = 'selected';
    }
    else {
      com = 'selected';
    }

    return (
      <footer id="footer">
        <span id="todo-count"><strong>{this.countItemActive()} items active</strong></span>
        <ul id="filters">
          <li>
            <a className={al} onClick={() => this.handleFilter('all')} role={"button"}>All</a>
          </li>
          <li>
            <a className={act} onClick={() => this.handleFilter('active')} role={"button"}>Active</a>
          </li>
          <li>
            <a className={com} onClick={() => this.handleFilter('completed')} role={"button"}>Completed</a>
          </li>
        </ul>
        {this.handleGetItemCompleted()}
      </footer>
    );
  }
}

export default Control;
