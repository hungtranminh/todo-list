import React, {Component} from 'react';
// import logo from './logo.svg';
// import './App.css';
import Form from "./components/Form";
import List from "./components/List";
import Control from "./components/Control";

const uuidv4 = require('uuid/v4');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        // {
        //   id: uuidv4(),
        //   content: 'a',
        //   complete: false
        // },
        // {
        //   id: uuidv4(),
        //   content: 'b',
        //   complete: false
        // }
      ],
      filterStatus: 'all',
      changeCompleted: false
    };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleComplete = this.handleComplete.bind(this);
    this.handleFilterStatus = this.handleFilterStatus.bind(this);
    this.handleClearCompleted = this.handleClearCompleted.bind(this);
    this.handleSetCompleted = this.handleSetCompleted.bind(this);
  }

  handleDelete(id) {
    const index = this.state.items.findIndex(item => item.id === id);
    if (index === -1)
      return;
    this.state.items.splice(index, 1);
    this.setState(this.state);
  }

  handleSubmit(item) {
    let items = this.state.items;
    items.push({
      id: uuidv4(),
      content: item.content,
      complete: false
    });
    this.setState(this.state);
  }

  handleComplete(item) {
    item.complete = !item.complete;
    this.setState(this.state);
  }

  handleFilterStatus(filterStatus) {
    this.setState({
      filterStatus: filterStatus,
    });
  }

  handleClearCompleted(arr) {
    for (let i of arr) {
      const index = this.state.items.findIndex(item => item.id === i.id);
      if (index === -1)
        return;
      this.state.items.splice(index, 1);
    }
    this.setState(this.state);
  }

  handleSetCompleted(arr) {
    if (this.state.changeCompleted === false) {
      for (let i of arr) {
        i.complete = true;
        this.setState(this.state);
      }
      this.setState({
        changeCompleted: !this.state.changeCompleted
      })
    }
    else {
      for (let i of arr) {
        i.complete = false;
        this.setState(this.state);
      }
      this.setState({
        changeCompleted: !this.state.changeCompleted
      })
    }
  }

  handleShowListControl() {
    let items = this.state.items;
    let filterStatus = this.state.filterStatus;
    if (this.state.items.length > 0) {
      return (
        <div>
          <List onClickComplete={this.handleComplete} onClickDelete={this.handleDelete}
                onClickSetCompleted={this.handleSetCompleted} items={items} filterStatus={filterStatus}/>
          <Control onClickFilterStatus={this.handleFilterStatus} onClickClearCompleted={this.handleClearCompleted}
                   items={items} filterStatus={filterStatus}/>
        </div>

      );
    }
  }

  render() {
    return (
      < div>
        <Form onClickSubmit={this.handleSubmit}/>
        {this.handleShowListControl()}
      </div>
    );
  }
}

export default App;
